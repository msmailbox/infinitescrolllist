package com.avantaj.android.product.component;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class TestScrollUpdateActivity extends Activity{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);    
        ListView listView = (ListView)findViewById(R.id.listView1);
        
        ArrayList<String> items = new ArrayList<String>();
        for (int i = 0; i < 10; i++){
        	items.add("Item "+i);	
        }
        
        TestScrollLoadingAdapter adapter = new TestScrollLoadingAdapter(this, android.R.layout.simple_list_item_1, items) ;
        listView.setAdapter(adapter);
        listView.setOnScrollListener(adapter);
        
    }   
}
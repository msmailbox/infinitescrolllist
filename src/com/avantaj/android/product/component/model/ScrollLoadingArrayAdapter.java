package com.avantaj.android.product.component.model;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public abstract class ScrollLoadingArrayAdapter<T> extends ArrayAdapter<T> implements OnScrollListener{
	
	/* true, if the list it waiting to load more data */
	private boolean isLoading = false;
	
	/* the table cell item in the bottom that is showing 'loading' text when a user reach the end of the list. */
	private TextView loadingView = null;
	
	
	public ScrollLoadingArrayAdapter(Context context, int textViewResourceId,
			List<T> objects) {
		super(context, textViewResourceId, objects);
		
	}
	
	/**
	 * Overridden the getCount. If the 'loading' item is shown, the count is count + 1 
	 */
	@Override
	public int getCount() {
		if(isLoading){
			return super.getCount() + 1;
		}else{
			return super.getCount();
		}
	}

	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		/* noting here */
	}
	
	/**
	 * Overridden getView to draw seperate view for normal item and 'loading item.'
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(isLoading && position == getCount() -1 ){
			return getLoadingView();
		}
		Log.d("Count = ", " "+getCount());
		View superView =  super.getView(position, convertView, parent);
		return getItemView(position, convertView, parent, superView);

	}
	
	
	/**
	 * Append more objects at the end of the list
	 * @param objects
	 */
	public void appendObjects(List<T> objects) {
		resetLoading();
		if(objects == null){
			return;
		}
		for (T t : objects) {	
			add(t);
		}
		notifyDataSetChanged();
	}
	
	/**
	 * sets loading to false, if the loading of the data is finished
	 */
	private void resetLoading(){
		
		isLoading = false;
		notifyDataSetChanged();
		
	}
	
	
	/**
	 * To get the event when user reaches at the end of the list. 
	 */
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		
		
		if(firstVisibleItem + visibleItemCount >= totalItemCount){
			/*
			 * Once user reaches the end of the list, further events are blocked by setting idLoading  = false
			 */
			if(!isLoading){
				isLoading = true;
				notifyDataSetChanged();
				/* Call this function for user to populate more data data to the list*/
				reachedListEnd(totalItemCount);
				
			}
			
		}
	}
	
	/**
	 * Returns the view of the item in the list. 
	 * @param position
	 * @param convertView
	 * @param parent
	 * @param superView Value of super.getView(). Should be the view given at constructor
	 * @return
	 */
	protected View getItemView(int position, View convertView, ViewGroup parent, View superView){
		TextView textView = new TextView(getContext());
		textView.setHeight(100);
		textView.setText("value"+position);
		return textView;
	}
	
	/**
	 * Value of the loading item shown at the end of the list
	 * @return
	 */
	protected View getLoadingView(){
		
		if (loadingView == null){
			loadingView = new TextView(getContext());
			loadingView.setHeight(30);
			loadingView.setText("Loading");
			loadingView.setBackgroundColor(0xFFFFFFFF);
		}
		
		return loadingView;
	}
	
	/**
	 * Override this function to implement the fetching of more data and setting it the list. 
	 * Call appendObjects(List<t> items) to append more objects. 
	 * @param itemCount Total number of items currently present in the list.
	 */
	
	protected abstract void reachedListEnd(int itemCount);

	
}

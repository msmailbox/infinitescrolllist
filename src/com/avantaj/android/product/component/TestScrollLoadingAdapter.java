package com.avantaj.android.product.component;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.avantaj.android.product.component.model.ScrollLoadingArrayAdapter;

public class TestScrollLoadingAdapter extends ScrollLoadingArrayAdapter<String> {

	public TestScrollLoadingAdapter(Context context, int textViewResourceId,
			List<String> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void reachedListEnd(int itemCount) {

		ArrayList<String> items = new ArrayList<String>();
		for(int i = itemCount; i < itemCount+10; i++){
			items.add("Item "+i);
		}
		appendObjects(items);
	
		
	}

}
